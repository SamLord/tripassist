![car-blue-icon[1].png](https://bitbucket.org/repo/gdE9GB/images/424350396-car-blue-icon%5B1%5D.png)

# Summary #

**PLEASE NOTE:**

In addition to the licence for this software, please note: In many locations it is illegal to use devices whilst driving, and even if it isn't where you are you may significantly impair your driving by not paying attention, eg. looking at a laptop plugged into your car. As such, it is strongly advised you only use this software as a passenger. Also, I'm just some guy on the internet as far as you're concerned and might want to turn on your brakes when you hit 70mph (I'm not, as it happens, but [bugs do happen](https://bitbucket.org/SamLord/tripassist/issues?status=new&status=open)) - **read and understand the source code** to understand what is happening to your car **before** plugging in anything.
That being said, I hope you find the software useful - forks and pull requests welcome.

Please see the licence at the bottom of this ReadMe before using this software! :)

### Current Stable Version! ##

On the [downloads](https://bitbucket.org/SamLord/tripassist/downloads/) page - V1.0

## Current Goals: ##

* Car trip software (Fault codes, speed, mpg, revs, supported PIDs (with description?) etc) - makes use of [Coding4Fun.Obd](https://obd.codeplex.com/) for its communication with the car - This portion of the source code is distributed under the [Microsoft Public License (Ms-PL)](https://opensource.org/licenses/MS-PL) see 'Licence' section. This is practically complete, all that's left to add is logging data to a CSV.
     
* ~~Console - Debug output and available ECUs and OBD II PIDs~~ - Done!
    

Stretch Goals:

* Sat/[Wifi Nav](https://bitbucket.org/SamLord/wifiloc)igation
* Podcast/Music system


System being developed using a Renault Clio Mk2, testing and feedback on other makes/models welcome -  please open an issue if you have feedback on this or anything else.


## Requirements ##

* Visual studio 2015 Community Edition or later (I imagine Ultimate, Professional etc also work fine)

* The contents of this repo!

* [CircularProgressBar.2.2.0](https://github.com/falahati/CircularProgressBar) (Available on NuGet)

* .NET 4/4.5.2

* For testing on cars an ELM327 adapter to plug your PC into your car - [This](https://www.amazon.co.uk/gp/product/B00IWLLX1Y) is the one I'm using on my car. There are [some websites](http://www.obdkey.com/featuredvehicle.asp) that can fill you in on what data you can expect to get from your vehicle. I'm not 100% on the accuracy of this data but it seems correct for my car.

## Donate ##

If I've helped you out and you're feeling generous you're welcome to buy me a drink:

# [![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GPTBHE69LQXJ2) #


### Licence ###

The contents of this repository, where not otherwise stated, are distributed under the MIT Licence:


```
#!txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

     The above copyright notice and this permission notice shall be included in
     all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```

The Coding4Fun.Obd portion of this source code is distributed under the MS-PL Licence:
```
#!txt

This license governs use of the accompanying software. If you use the software, you
accept this license. If you do not accept the license, do not use the software.

1. Definitions
The terms "reproduce," "reproduction," "derivative works," and "distribution" have the
same meaning here as under U.S. copyright law.
A "contribution" is the original software, or any additions or changes to the software.
A "contributor" is any person that distributes its contribution under this license.
"Licensed patents" are a contributor's patent claims that read directly on its contribution.

2. Grant of Rights
(A) Copyright Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, each contributor grants you a non-exclusive, worldwide, royalty-free copyright license to reproduce its contribution, prepare derivative works of its contribution, and distribute its contribution or any derivative works that you create.
(B) Patent Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, each contributor grants you a non-exclusive, worldwide, royalty-free license under its licensed patents to make, have made, use, sell, offer for sale, import, and/or otherwise dispose of its contribution in the software or derivative works of the contribution in the software.

3. Conditions and Limitations
(A) No Trademark License- This license does not grant you rights to use any contributors' name, logo, or trademarks.
(B) If you bring a patent claim against any contributor over patents that you claim are infringed by the software, your patent license from such contributor to the software ends automatically.
(C) If you distribute any portion of the software, you must retain all copyright, patent, trademark, and attribution notices that are present in the software.
(D) If you distribute any portion of the software in source code form, you may do so only under this license by including a complete copy of this license with your distribution. If you distribute any portion of the software in compiled or object code form, you may only do so under a license that complies with this license.
(E) The software is licensed "as-is." You bear the risk of using it. The contributors give no express warranties, guarantees or conditions. You may have additional consumer rights under your local laws which this license cannot change. To the extent permitted under your local laws, the contributors exclude the implied warranties of merchantability, fitness for a particular purpose and non-infringement.
```
