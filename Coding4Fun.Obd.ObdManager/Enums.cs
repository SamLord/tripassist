﻿namespace Coding4Fun.Obd.ObdManager
{
	public enum Protocol
	{
		Unknown = -1,
		ElmAutomatic = 0,
		SaeJ1850Pwm,
		SaeJ1850Vpw,
		Iso91412, //implemented
		Iso142304Kwp5Baud104Kbaud, //https://law.resource.org/pub/us/cfr/ibr/004/iso.14230-4.2000.pdf
        Iso142304KwpFast104Kbaud, //implemented
		Iso157654Can11Bit500Kbaud, //implemented
		Iso157654Can29Bit500Kbaud, //implemented
		Iso157654Can11Bit250Kbaud,
		Iso157654Can29Bit250Kbaud,
        //There are another 3 protocols - SAE J1939 CAN, USER1 CAN, USER2 CAN - however, implementing them requires changing the backing values here to be interpreted at hex as they are A, B, C respectively
        //They are also the only ones to allow user-defined baud rates - see page 26 here https://www.elmelectronics.com/wp-content/uploads/2016/07/ELM327DS.pdf
    }
}
