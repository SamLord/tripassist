﻿using Coding4Fun.Obd.ObdManager;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripAssist.Core.CarConnection
{
    class SerialMonitor
    {
        string LastResponse;
        SerialPort _serial;
        int _errorCount;
        bool _connected = false;
        Task monitorTask;

        public SerialMonitor(string comPort, int baud, int protocol)
        {
            _serial = new SerialPort(comPort, baud);

            _serial.WriteTimeout = 10000; //10s
            _serial.ReadTimeout = 10000; //10s

            string lineEndString = ">";
            _serial.NewLine = lineEndString;      // responses end with the > prompt character
            _serial.Open();
            Console.WriteLine("Serial connection opened");

            _errorCount = 0;
            _connected = true;


            Console.WriteLine("Sending intital setup events");
            LastResponse = WriteAndCheckResponse("ATZ"); // reset
            LastResponse = WriteAndCheckResponse("ATE0"); // echo off
            LastResponse = WriteAndCheckResponse("ATL0"); // line feeds off

            LastResponse = WriteAndCheckResponse("ATSP" + protocol); // OBD protocol
            LastResponse = WriteAndCheckResponse("01 00");  // send command to initialize comm bus

            Console.WriteLine("Init complete... entering monitor mode");
            WriteAndCheckResponse("AT MA");

            monitorTask = Task.Run(async () =>
            {
                while (_connected)
                {
                    Task<string> readTask = ReadUntil("\n");
                    readTask.Wait();
                    Console.WriteLine(readTask.Result);
                    await Task.Delay(500);
                }
            });
        }

        public void Disconnect()
        {
            _connected = false;
            if(monitorTask != null)
            {
                monitorTask.Wait();
            }
            WriteAndCheckResponse("ATZ"); //reset device
            _serial.Close(); //close serial connection
        }

        private async Task<string> ReadUntil(string messageEnd)
        {
            string currentMessageEnd = _serial.NewLine;
            _serial.NewLine = messageEnd;

            string readLine = _serial.ReadLine();

            _serial.NewLine = currentMessageEnd;

            return readLine;
        }

        private string WriteAndCheckResponse(string line)
        {
            WriteLine(line);
            string response = _serial.ReadLine();
            System.Diagnostics.Debug.Write(line + ", " + response);
            if (!response.Contains("41") && !response.Contains("OK") && !response.Contains("ELM") && !response.Contains("SEARCHING"))
                throw new ObdException(string.Format("Error initializing OBD: {0} - {1}. Check protocol settings.", line, response));
            return response;
        }

        private void WriteLine(string line)
        {
            _serial.Write(line + "\r");
        }
    }
}
