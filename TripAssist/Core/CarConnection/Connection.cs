﻿using Coding4Fun.Obd.ObdManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripAssist.Core.ObserverPattern;

namespace TripAssist.Core.CarConnection
{
    public class Connection : ISubject
    {
        private ConnectionInfo connectionInfo;
        private bool connected;
        private ObdDevice device;
        private ObdState currentCarState;

        private List<IObserver> observers;

        public bool Connected
        {
            get
            {
                return connected;
            }
        }

        public ObdState CurrentCarState
        {
            get
            {
                return currentCarState;
            }
        }

        public string SupportedPIDs
        {
            get
            {
                if (connected)
                {
                    string output = "";

                    foreach(KeyValuePair<int,List<int>> keyValPair in device.SupportedPIDs)
                    {
                        output += "ECU " + keyValPair.Key.ToString() + " supports:" + Environment.NewLine + "|";
                        foreach(int value in keyValPair.Value)
                        {
                            output += value.ToString() + " | ";
                        }
                        output += Environment.NewLine;
                    }

                    return output;
                }
                else
                {
                    return "Not connected - no supported PIDs available";
                }
            }
        }

        public Connection(int comPort, int baud, Protocol protocol)
        {
            Setup(new ConnectionInfo(comPort, baud, protocol));
        }

        public Connection(ConnectionInfo connectionInfo)
        {
            Setup(connectionInfo);
        }

        private void Setup(ConnectionInfo connectionInfo)
        {
            this.connectionInfo = connectionInfo;
            connected = false;
            device = new ObdDevice();
            currentCarState = new ObdState();
            observers = new List<IObserver>();
            device.ObdChanged += OBDUpdate;
        }

        public bool Connect()
        {
            if (!connected)
            {
                connected = false;
                try
                {
                    device.Connect(connectionInfo.Port, connectionInfo.BaudRate, (int)connectionInfo.Protocol, true);
                    connected = true;
                }
                catch (System.IO.IOException ex)
                {
                    Console.WriteLine("CONNECTION FAILURE (IOException) " + ex.Message + ", " + ex.StackTrace);
                }
                catch (NullReferenceException ex)
                {
                    Console.WriteLine("CONNECTION FAILURE: (NullReferenceException) " + ex.Message + ", " + ex.StackTrace);
                }
                catch (System.TimeoutException ex)
                {
                    Console.WriteLine("CONNECTION FAILURE: (TimeoutException) " + ex.Message + ", " + ex.StackTrace);
                }
                catch (ObdException ex)
                {
                    Console.WriteLine("CONNECTION FAILURE: (ObdException) " + ex.Message + ", " + ex.StackTrace);
                }
                finally
                {
                    //If connection failed at some point, ensure the serial port is freed up for another attempt
                    if (!connected)
                    {
                        Disconnect(true);
                    }
                }
                
                Console.WriteLine("Connected: " + connected);
                return connected;
            }
            else
            {
                Console.WriteLine("Warning! Tried to connect when already connected");
                return false;
            }
        }

        public void Disconnect(bool force = false)
        {
            if (connected || force)
            {
                device.Disconnect();
                Console.WriteLine("Disconnected!");
            }
        }

        private void OBDUpdate(object sender, ObdChangedEventArgs args)
        {
            currentCarState = args.ObdState;
            NotifyObservers();
        }

        #region ObserverPattern
        public void AddObserver(IObserver observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        private void NotifyObservers()
        {
            foreach(IObserver observer in observers)
            {
                observer.notify(this);
            }
        }
        #endregion

        public override string ToString()
        {
            return currentCarState.ToString() + Environment.NewLine
                + connectionInfo.Port + ", @ " + connectionInfo.BaudRate + ", prtcl " + connectionInfo.Protocol.ToString()
                + " (#" + (int)connectionInfo.Protocol + ")";
        }
    }
}
