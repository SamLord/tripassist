﻿using Coding4Fun.Obd.ObdManager;

namespace TripAssist.Core.CarConnection
{
    public class ConnectionInfo
    {
        public string Port { get; set; }
        public int BaudRate { get; set; }
        public Protocol Protocol { get; set; }

        public ConnectionInfo(int comPort, int baud, Protocol protocol)
        {
            Port = "COM" + comPort;
            BaudRate = baud;
            Protocol = protocol;
        }
    }
}
