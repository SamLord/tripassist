﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripAssist.Core.ObserverPattern
{
    public interface IObserver
    {
        void notify(ISubject subject);
    }
}
