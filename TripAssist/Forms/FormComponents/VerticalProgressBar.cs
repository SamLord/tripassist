﻿using System;
using System.Windows.Forms; 

namespace TripAssist.Forms.FormComponents
{
    public class VerticalProgressBar : ProgressBar
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style |= 0x04;

                //flip width.height
                if (cp.Width > cp.Height)
                {
                    int tempWidth = cp.Height;
                    cp.Height = cp.Width;
                    cp.Width = tempWidth;
                }

                return cp;
            }
        }
    }
}
