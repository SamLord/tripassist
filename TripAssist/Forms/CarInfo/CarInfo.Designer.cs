﻿namespace TripAssist.Forms.CarInfo
{
    partial class CarInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectDisconnectButton = new System.Windows.Forms.Button();
            this.portLabel = new System.Windows.Forms.Label();
            this.baudRateLabel = new System.Windows.Forms.Label();
            this.protocolTextBox = new System.Windows.Forms.TextBox();
            this.protocolLabel = new System.Windows.Forms.Label();
            this.dashboardButton = new System.Windows.Forms.Button();
            this.comPortComboBox = new System.Windows.Forms.ComboBox();
            this.baudRateComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // connectDisconnectButton
            // 
            this.connectDisconnectButton.Location = new System.Drawing.Point(9, 67);
            this.connectDisconnectButton.Margin = new System.Windows.Forms.Padding(2);
            this.connectDisconnectButton.Name = "connectDisconnectButton";
            this.connectDisconnectButton.Size = new System.Drawing.Size(75, 81);
            this.connectDisconnectButton.TabIndex = 0;
            this.connectDisconnectButton.Text = "Connect";
            this.connectDisconnectButton.UseVisualStyleBackColor = true;
            this.connectDisconnectButton.Click += new System.EventHandler(this.connectDisconnectButton_Click);
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(8, 7);
            this.portLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(63, 13);
            this.portLabel.TabIndex = 3;
            this.portLabel.Text = "COM Port #";
            // 
            // baudRateLabel
            // 
            this.baudRateLabel.AutoSize = true;
            this.baudRateLabel.Location = new System.Drawing.Point(75, 7);
            this.baudRateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.baudRateLabel.Name = "baudRateLabel";
            this.baudRateLabel.Size = new System.Drawing.Size(58, 13);
            this.baudRateLabel.TabIndex = 4;
            this.baudRateLabel.Text = "Baud Rate";
            // 
            // protocolTextBox
            // 
            this.protocolTextBox.Location = new System.Drawing.Point(140, 24);
            this.protocolTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.protocolTextBox.Name = "protocolTextBox";
            this.protocolTextBox.Size = new System.Drawing.Size(53, 20);
            this.protocolTextBox.TabIndex = 5;
            this.protocolTextBox.Text = "5";
            this.protocolTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // protocolLabel
            // 
            this.protocolLabel.AutoSize = true;
            this.protocolLabel.Location = new System.Drawing.Point(137, 7);
            this.protocolLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.protocolLabel.Name = "protocolLabel";
            this.protocolLabel.Size = new System.Drawing.Size(56, 13);
            this.protocolLabel.TabIndex = 6;
            this.protocolLabel.Text = "Protocol #";
            // 
            // dashboardButton
            // 
            this.dashboardButton.Location = new System.Drawing.Point(89, 67);
            this.dashboardButton.Margin = new System.Windows.Forms.Padding(2);
            this.dashboardButton.Name = "dashboardButton";
            this.dashboardButton.Size = new System.Drawing.Size(75, 81);
            this.dashboardButton.TabIndex = 7;
            this.dashboardButton.Text = "Show Dashboard";
            this.dashboardButton.UseVisualStyleBackColor = true;
            this.dashboardButton.Click += new System.EventHandler(this.dashboardButton_Click);
            // 
            // comPortComboBox
            // 
            this.comPortComboBox.FormattingEnabled = true;
            this.comPortComboBox.Location = new System.Drawing.Point(9, 23);
            this.comPortComboBox.Name = "comPortComboBox";
            this.comPortComboBox.Size = new System.Drawing.Size(62, 21);
            this.comPortComboBox.TabIndex = 8;
            // 
            // baudRateComboBox
            // 
            this.baudRateComboBox.FormattingEnabled = true;
            this.baudRateComboBox.Location = new System.Drawing.Point(78, 24);
            this.baudRateComboBox.Name = "baudRateComboBox";
            this.baudRateComboBox.Size = new System.Drawing.Size(55, 21);
            this.baudRateComboBox.TabIndex = 9;
            // 
            // CarInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 206);
            this.Controls.Add(this.baudRateComboBox);
            this.Controls.Add(this.comPortComboBox);
            this.Controls.Add(this.dashboardButton);
            this.Controls.Add(this.protocolLabel);
            this.Controls.Add(this.protocolTextBox);
            this.Controls.Add(this.baudRateLabel);
            this.Controls.Add(this.portLabel);
            this.Controls.Add(this.connectDisconnectButton);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CarInfo";
            this.Text = "CarInfo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CarInfo_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectDisconnectButton;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.Label baudRateLabel;
        private System.Windows.Forms.TextBox protocolTextBox;
        private System.Windows.Forms.Label protocolLabel;
        private System.Windows.Forms.Button dashboardButton;
        private System.Windows.Forms.ComboBox comPortComboBox;
        private System.Windows.Forms.ComboBox baudRateComboBox;
    }
}