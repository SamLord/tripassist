﻿namespace TripAssist.Forms.CarInfo
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.speedGauge = new CircularProgressBar.CircularProgressBar();
            this.rpmGauge = new CircularProgressBar.CircularProgressBar();
            this.fuelGauge = new CircularProgressBar.CircularProgressBar();
            this.throttlePositionValueLabel = new System.Windows.Forms.Label();
            this.throttlePositionTitleLabel = new System.Windows.Forms.Label();
            this.gaugeTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.coolantGroup = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.coolantDisplayBar = new TripAssist.Forms.FormComponents.VerticalProgressBar();
            this.milPictureBox = new System.Windows.Forms.PictureBox();
            this.milesWithMilValueLabel = new System.Windows.Forms.Label();
            this.coolantTempValLabel = new System.Windows.Forms.Label();
            this.gaugeTableLayout.SuspendLayout();
            this.coolantGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.milPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // speedGauge
            // 
            this.speedGauge.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.speedGauge.AnimationFunction = ((WinFormAnimation.AnimationFunctions.Function)(resources.GetObject("speedGauge.AnimationFunction")));
            this.speedGauge.AnimationSpeed = 1000;
            this.speedGauge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(60)))), ((int)(((byte)(166)))));
            this.speedGauge.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold);
            this.speedGauge.ForeColor = System.Drawing.Color.White;
            this.speedGauge.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(60)))), ((int)(((byte)(166)))));
            this.speedGauge.InnerMargin = 2;
            this.speedGauge.InnerWidth = -1;
            this.speedGauge.Location = new System.Drawing.Point(3, 26);
            this.speedGauge.Margin = new System.Windows.Forms.Padding(2);
            this.speedGauge.MarqueeAnimationSpeed = 2000;
            this.speedGauge.Maximum = 160;
            this.speedGauge.MinimumSize = new System.Drawing.Size(136, 136);
            this.speedGauge.Name = "speedGauge";
            this.speedGauge.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(20)))), ((int)(((byte)(36)))));
            this.speedGauge.OuterMargin = -25;
            this.speedGauge.OuterWidth = 26;
            this.speedGauge.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(89)))), ((int)(((byte)(217)))));
            this.speedGauge.ProgressWidth = 25;
            this.speedGauge.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speedGauge.Size = new System.Drawing.Size(136, 136);
            this.speedGauge.StartAngle = 270;
            this.speedGauge.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.speedGauge.SubscriptColor = System.Drawing.Color.White;
            this.speedGauge.SubscriptMargin = new System.Windows.Forms.Padding(0);
            this.speedGauge.SubscriptText = "mph";
            this.speedGauge.SuperscriptColor = System.Drawing.Color.White;
            this.speedGauge.SuperscriptMargin = new System.Windows.Forms.Padding(0);
            this.speedGauge.SuperscriptText = "";
            this.speedGauge.TabIndex = 0;
            this.speedGauge.Text = " 10";
            this.speedGauge.TextMargin = new System.Windows.Forms.Padding(0);
            this.speedGauge.Value = 10;
            // 
            // rpmGauge
            // 
            this.rpmGauge.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rpmGauge.AnimationFunction = ((WinFormAnimation.AnimationFunctions.Function)(resources.GetObject("rpmGauge.AnimationFunction")));
            this.rpmGauge.AnimationSpeed = 400;
            this.rpmGauge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(60)))), ((int)(((byte)(166)))));
            this.rpmGauge.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold);
            this.rpmGauge.ForeColor = System.Drawing.Color.White;
            this.rpmGauge.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(60)))), ((int)(((byte)(166)))));
            this.rpmGauge.InnerMargin = 2;
            this.rpmGauge.InnerWidth = -1;
            this.rpmGauge.Location = new System.Drawing.Point(145, 26);
            this.rpmGauge.Margin = new System.Windows.Forms.Padding(2);
            this.rpmGauge.MarqueeAnimationSpeed = 2000;
            this.rpmGauge.Maximum = 7500;
            this.rpmGauge.MinimumSize = new System.Drawing.Size(136, 136);
            this.rpmGauge.Name = "rpmGauge";
            this.rpmGauge.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(20)))), ((int)(((byte)(36)))));
            this.rpmGauge.OuterMargin = -25;
            this.rpmGauge.OuterWidth = 26;
            this.rpmGauge.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(89)))), ((int)(((byte)(217)))));
            this.rpmGauge.ProgressWidth = 25;
            this.rpmGauge.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.rpmGauge.Size = new System.Drawing.Size(136, 136);
            this.rpmGauge.StartAngle = 270;
            this.rpmGauge.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.rpmGauge.SubscriptColor = System.Drawing.Color.White;
            this.rpmGauge.SubscriptMargin = new System.Windows.Forms.Padding(0);
            this.rpmGauge.SubscriptText = "rpm";
            this.rpmGauge.SuperscriptColor = System.Drawing.Color.White;
            this.rpmGauge.SuperscriptMargin = new System.Windows.Forms.Padding(0);
            this.rpmGauge.SuperscriptText = "";
            this.rpmGauge.TabIndex = 1;
            this.rpmGauge.Text = "1258";
            this.rpmGauge.TextMargin = new System.Windows.Forms.Padding(0);
            this.rpmGauge.Value = 1258;
            // 
            // fuelGauge
            // 
            this.fuelGauge.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.fuelGauge.AnimationFunction = ((WinFormAnimation.AnimationFunctions.Function)(resources.GetObject("fuelGauge.AnimationFunction")));
            this.fuelGauge.AnimationSpeed = 1000;
            this.fuelGauge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(60)))), ((int)(((byte)(166)))));
            this.gaugeTableLayout.SetColumnSpan(this.fuelGauge, 2);
            this.fuelGauge.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fuelGauge.ForeColor = System.Drawing.Color.White;
            this.fuelGauge.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(60)))), ((int)(((byte)(166)))));
            this.fuelGauge.InnerMargin = 2;
            this.fuelGauge.InnerWidth = -1;
            this.fuelGauge.Location = new System.Drawing.Point(89, 191);
            this.fuelGauge.Margin = new System.Windows.Forms.Padding(2);
            this.fuelGauge.MarqueeAnimationSpeed = 2000;
            this.fuelGauge.MinimumSize = new System.Drawing.Size(105, 105);
            this.fuelGauge.Name = "fuelGauge";
            this.fuelGauge.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(20)))), ((int)(((byte)(36)))));
            this.fuelGauge.OuterMargin = -25;
            this.fuelGauge.OuterWidth = 26;
            this.fuelGauge.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(89)))), ((int)(((byte)(217)))));
            this.fuelGauge.ProgressWidth = 25;
            this.fuelGauge.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fuelGauge.Size = new System.Drawing.Size(105, 105);
            this.fuelGauge.StartAngle = 270;
            this.fuelGauge.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.fuelGauge.SubscriptColor = System.Drawing.Color.White;
            this.fuelGauge.SubscriptMargin = new System.Windows.Forms.Padding(0);
            this.fuelGauge.SubscriptText = " %";
            this.fuelGauge.SuperscriptColor = System.Drawing.Color.White;
            this.fuelGauge.SuperscriptMargin = new System.Windows.Forms.Padding(0);
            this.fuelGauge.SuperscriptText = " ";
            this.fuelGauge.TabIndex = 2;
            this.fuelGauge.Text = "Fuel";
            this.fuelGauge.TextMargin = new System.Windows.Forms.Padding(0);
            this.fuelGauge.Value = 68;
            // 
            // throttlePositionValueLabel
            // 
            this.throttlePositionValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.throttlePositionValueLabel.AutoSize = true;
            this.throttlePositionValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.throttlePositionValueLabel.ForeColor = System.Drawing.Color.White;
            this.throttlePositionValueLabel.Location = new System.Drawing.Point(280, 483);
            this.throttlePositionValueLabel.Name = "throttlePositionValueLabel";
            this.throttlePositionValueLabel.Size = new System.Drawing.Size(16, 18);
            this.throttlePositionValueLabel.TabIndex = 6;
            this.throttlePositionValueLabel.Text = "?";
            // 
            // throttlePositionTitleLabel
            // 
            this.throttlePositionTitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.throttlePositionTitleLabel.AutoSize = true;
            this.throttlePositionTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.throttlePositionTitleLabel.ForeColor = System.Drawing.Color.White;
            this.throttlePositionTitleLabel.Location = new System.Drawing.Point(176, 465);
            this.throttlePositionTitleLabel.Name = "throttlePositionTitleLabel";
            this.throttlePositionTitleLabel.Size = new System.Drawing.Size(120, 18);
            this.throttlePositionTitleLabel.TabIndex = 5;
            this.throttlePositionTitleLabel.Text = "Throttle Position:";
            // 
            // gaugeTableLayout
            // 
            this.gaugeTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gaugeTableLayout.ColumnCount = 2;
            this.gaugeTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gaugeTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gaugeTableLayout.Controls.Add(this.rpmGauge, 1, 0);
            this.gaugeTableLayout.Controls.Add(this.fuelGauge, 0, 1);
            this.gaugeTableLayout.Controls.Add(this.speedGauge, 0, 0);
            this.gaugeTableLayout.Location = new System.Drawing.Point(12, 12);
            this.gaugeTableLayout.MinimumSize = new System.Drawing.Size(284, 290);
            this.gaugeTableLayout.Name = "gaugeTableLayout";
            this.gaugeTableLayout.RowCount = 2;
            this.gaugeTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gaugeTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gaugeTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.gaugeTableLayout.Size = new System.Drawing.Size(284, 378);
            this.gaugeTableLayout.TabIndex = 8;
            this.gaugeTableLayout.SizeChanged += new System.EventHandler(this.gaugeTableLayout_SizeChanged);
            // 
            // coolantGroup
            // 
            this.coolantGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.coolantGroup.Controls.Add(this.coolantTempValLabel);
            this.coolantGroup.Controls.Add(this.pictureBox1);
            this.coolantGroup.Controls.Add(this.coolantDisplayBar);
            this.coolantGroup.Location = new System.Drawing.Point(85, 395);
            this.coolantGroup.Name = "coolantGroup";
            this.coolantGroup.Size = new System.Drawing.Size(91, 111);
            this.coolantGroup.TabIndex = 10;
            this.coolantGroup.TabStop = false;
            this.coolantGroup.Text = "Coolant";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TripAssist.Properties.Resources.Coolant;
            this.pictureBox1.Location = new System.Drawing.Point(6, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(23, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // coolantDisplayBar
            // 
            this.coolantDisplayBar.Location = new System.Drawing.Point(6, 57);
            this.coolantDisplayBar.Maximum = 255;
            this.coolantDisplayBar.Name = "coolantDisplayBar";
            this.coolantDisplayBar.Size = new System.Drawing.Size(23, 48);
            this.coolantDisplayBar.TabIndex = 9;
            this.coolantDisplayBar.Value = 12;
            // 
            // milPictureBox
            // 
            this.milPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.milPictureBox.Image = global::TripAssist.Properties.Resources.MIL_off;
            this.milPictureBox.Location = new System.Drawing.Point(12, 471);
            this.milPictureBox.Name = "milPictureBox";
            this.milPictureBox.Size = new System.Drawing.Size(41, 35);
            this.milPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.milPictureBox.TabIndex = 7;
            this.milPictureBox.TabStop = false;
            // 
            // milesWithMilValueLabel
            // 
            this.milesWithMilValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.milesWithMilValueLabel.AutoSize = true;
            this.milesWithMilValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.milesWithMilValueLabel.ForeColor = System.Drawing.Color.White;
            this.milesWithMilValueLabel.Location = new System.Drawing.Point(9, 450);
            this.milesWithMilValueLabel.Name = "milesWithMilValueLabel";
            this.milesWithMilValueLabel.Size = new System.Drawing.Size(37, 18);
            this.milesWithMilValueLabel.TabIndex = 11;
            this.milesWithMilValueLabel.Text = "0km";
            // 
            // coolantTempValLabel
            // 
            this.coolantTempValLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.coolantTempValLabel.AutoSize = true;
            this.coolantTempValLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coolantTempValLabel.ForeColor = System.Drawing.Color.White;
            this.coolantTempValLabel.Location = new System.Drawing.Point(36, 87);
            this.coolantTempValLabel.Name = "coolantTempValLabel";
            this.coolantTempValLabel.Size = new System.Drawing.Size(33, 18);
            this.coolantTempValLabel.TabIndex = 12;
            this.coolantTempValLabel.Text = "0°C";
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(60)))), ((int)(((byte)(166)))));
            this.ClientSize = new System.Drawing.Size(308, 518);
            this.Controls.Add(this.milesWithMilValueLabel);
            this.Controls.Add(this.coolantGroup);
            this.Controls.Add(this.gaugeTableLayout);
            this.Controls.Add(this.milPictureBox);
            this.Controls.Add(this.throttlePositionValueLabel);
            this.Controls.Add(this.throttlePositionTitleLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(324, 469);
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Dashboard_FormClosing);
            this.gaugeTableLayout.ResumeLayout(false);
            this.coolantGroup.ResumeLayout(false);
            this.coolantGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.milPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CircularProgressBar.CircularProgressBar speedGauge;
        private CircularProgressBar.CircularProgressBar rpmGauge;
        private CircularProgressBar.CircularProgressBar fuelGauge;
        private System.Windows.Forms.Label throttlePositionValueLabel;
        private System.Windows.Forms.Label throttlePositionTitleLabel;
        private System.Windows.Forms.PictureBox milPictureBox;
        private System.Windows.Forms.TableLayoutPanel gaugeTableLayout;
        private FormComponents.VerticalProgressBar coolantDisplayBar;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox coolantGroup;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label milesWithMilValueLabel;
        private System.Windows.Forms.Label coolantTempValLabel;
    }
}