﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TripAssist.Core.CarConnection;
using TripAssist.Core.ObserverPattern;

namespace TripAssist.Forms.CarInfo
{
    public partial class Dashboard : Form, IObserver
    {
        private Connection connection;
        private Size lastFormSize;

        public Dashboard(Core.CarConnection.Connection connection)
        {
            InitializeComponent();
            lastFormSize = this.Size;
            //SetRPM(2300);
            //SetSpeed(76, "km/h");
            //SetFuel(15);
            this.connection = connection;
            connection.AddObserver(this);
        }

        private void SetRPM(int rpm)
        {
            if (rpm < rpmGauge.Minimum)
            {
                Console.WriteLine("Dashboard: Ignored invalid RPM value - " + rpm);
                return;
            }

            rpmGauge.Value = rpm;
            rpmGauge.Text = rpm.ToString();
        }

        private void SetSpeed(int speed, string units = "m/h")
        {
            if (speed < speedGauge.Minimum)
            {
                Console.WriteLine("Dashboard: Ignored invalid speed value - " + speed);
                return;
            }

            speedGauge.Value = speed;
            speedGauge.Text = speed.ToString();
            speedGauge.SuperscriptText = units;
        }

        private void SetFuel(int fuelPercentage)
        {
            if (fuelPercentage >= 0 && fuelPercentage <= 100)
            {
                fuelGauge.Value = fuelPercentage;
            }
            else
            {
                fuelGauge.Value = 0;
            }
        }

        private void SetThrottlePosition(int throttlePosition)
        {
            if (throttlePosition >= 0 && throttlePosition <= 100)
            {
                throttlePositionValueLabel.Text = throttlePosition.ToString() + "%";
            }
            else
            {
                Console.WriteLine("Dashboard: Invalid fuel value - " + throttlePosition);
                throttlePositionValueLabel.Text = "?";
            }
        }

        private void SetMIL(bool active, int distance)
        {
            if (active)
            {
                milPictureBox.Image = Properties.Resources.MIL_on;
                milesWithMilValueLabel.Text = distance + " km";
                milesWithMilValueLabel.Enabled = true;
            }
            else
            {
                milPictureBox.Image = Properties.Resources.MIL_off;
                milesWithMilValueLabel.Enabled = false;
            }
            milPictureBox.Refresh();
        }

        private void SetCoolantTemp(int temperature, string units = "°C")
        {
            //Valid temperatures are -40 -> 215
            if (temperature < -40)
            {
                temperature = -40;
            }
            if(temperature > 215)
            {
                temperature = 215;
            }
            //Display bar lowest val possible is 0... so we add 40 to the value we were given
            coolantDisplayBar.Value = temperature + 40;
            //Then display actual value as text
            coolantTempValLabel.Text = temperature + units;
        }

        private void Dashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        /// <summary>
        /// This is called when the Connection gets updated info from the car
        /// </summary>
        /// <param name="subject"></param>
        public void notify(ISubject subject)
        {
            if (subject.GetType() == typeof(Connection))
            {
                Connection notifier = (Connection)subject;
                if (notifier == null)
                {
                    Console.WriteLine("Dashboard notification failed as connection is null");
                    return;
                }

                SetSpeed(notifier.CurrentCarState.MilesPerHour);
                SetFuel(notifier.CurrentCarState.FuelLevel);
                SetRPM(notifier.CurrentCarState.Rpm);
                SetCoolantTemp(notifier.CurrentCarState.EngineCoolantTemperature);
                SetThrottlePosition(notifier.CurrentCarState.ThrottlePosition);
                SetMIL(notifier.CurrentCarState.MilLightOn, notifier.CurrentCarState.DistanceTraveledWithMilOn);
            }
        }

        private void gaugeTableLayout_SizeChanged(object sender, EventArgs e)
        {
            Size newFormSize = gaugeTableLayout.Size;
            Size minSize = gaugeTableLayout.MinimumSize;
            float widthScale = (float)newFormSize.Width / (float)minSize.Width;
            float heightScale = (float)newFormSize.Height / (float)minSize.Height;

            if (widthScale < heightScale)
            {
                speedGauge.Size = ScaleSize(speedGauge.MinimumSize, widthScale);
                rpmGauge.Size = ScaleSize(rpmGauge.MinimumSize, widthScale);
                fuelGauge.Size = ScaleSize(fuelGauge.MinimumSize, widthScale);

                ResizeFont(gaugeTableLayout.Controls, (float)newFormSize.Width / (float)lastFormSize.Width);
            }
            else
            {
                speedGauge.Size = ScaleSize(speedGauge.MinimumSize, heightScale);
                rpmGauge.Size = ScaleSize(rpmGauge.MinimumSize, heightScale);
                fuelGauge.Size = ScaleSize(fuelGauge.MinimumSize, heightScale);

                ResizeFont(gaugeTableLayout.Controls, (float)newFormSize.Height / (float)lastFormSize.Height);
            }
            lastFormSize = newFormSize;
        }

        private void ResizeFont(Control.ControlCollection controlCollection, float scaleFactor)
        {
            foreach (Control control in controlCollection)
            {
                if (control.HasChildren)
                {
                    ResizeFont(control.Controls, scaleFactor);
                }
                else
                {
                    // scale font
                    control.Font = new Font(control.Font.FontFamily.Name, control.Font.Size * scaleFactor);

                    //special case: circular progress bar sub + super script
                    if (control.GetType() == typeof(CircularProgressBar.CircularProgressBar))
                    {
                        CircularProgressBar.CircularProgressBar progressBarControl = (CircularProgressBar.CircularProgressBar)control;
                        progressBarControl.SecondaryFont = new Font(progressBarControl.SecondaryFont.FontFamily.Name, progressBarControl.SecondaryFont.Size * scaleFactor);
                    }
                }
            }
        }

        private Size ScaleSize(Size size, float scale)
        {
            Size newSize = new Size();
            newSize.Height = (int)(size.Height * scale);
            newSize.Width = (int)(size.Width * scale);
            return newSize;
        }
    }
}
