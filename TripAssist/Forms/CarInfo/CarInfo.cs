﻿using Coding4Fun.Obd.ObdManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TripAssist.Core.CarConnection;

namespace TripAssist.Forms.CarInfo
{
    public partial class CarInfo : Form
    {
        private bool connected;
        private Connection carConnection;

        private Dashboard dashboard;
        private SerialMonitor serialMonitor;

        public CarInfo()
        {
            InitializeComponent();
            
            //add available com ports to drop down as numbers
            comPortComboBox.DataSource = System.IO.Ports.SerialPort.GetPortNames().Select(x => System.Text.RegularExpressions.Regex.Match(x, @"\d+").Value).ToArray();

            //add available baud rates to baud drop down
            baudRateComboBox.DataSource = new int[] { 110, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 57600, 115200, 128000, 256000 };

            connected = false;
            UpdateButtons();
        }

        private void CarInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void connectDisconnectButton_Click(object sender, EventArgs e)
        {
            if (!connected)
            {
                carConnection = new Connection(int.Parse((string)comPortComboBox.Text), int.Parse(baudRateComboBox.Text),
                    (Protocol)int.Parse(protocolTextBox.Text));
                Console.WriteLine("Ready to connect, connection info: " + carConnection);
                connected = carConnection.Connect();

                if (connected)
                {
                    UpdateButtons();
                    Console.WriteLine(carConnection.SupportedPIDs);
                }
                else
                {
                    connectDisconnectButton.Text = "Fail, Try connect again?";
                }
            }
            else
            {
                connected = false;
                carConnection.Disconnect();
                dashboard.Close();
                dashboard = null;
                UpdateButtons();
            }
        }

        private void UpdateButtons()
        {
            if (connected)
            {
                foreach (Control control in this.Controls)
                {
                    if(control.GetType() == typeof(Button))
                    {
                        control.Enabled = true;
                    }
                }
                connectDisconnectButton.Text = "Disconnect";
            }
            else
            {
                foreach (Control control in this.Controls)
                {
                    if (control.GetType() == typeof(Button))
                    {
                        control.Enabled = false;
                    }
                }
                connectDisconnectButton.Enabled = true;
                connectDisconnectButton.Text = "Connect";
            }
        }

        private void dashboardButton_Click(object sender, EventArgs e)
        {
            if(dashboard == null)
            {
                dashboard = new Dashboard(carConnection);
            }

            dashboard.Show();
        }

        //private void monitorModeButton_Click(object sender, EventArgs e)
        //{
        //    if(serialMonitor != null)
        //    {
        //        serialMonitor.Disconnect();
        //    }
        //    int comPort = int.Parse((string)comPortComboBox.Text);
        //    serialMonitor = new SerialMonitor("COM" + comPort, int.Parse(baudRateComboBox.Text), int.Parse(protocolTextBox.Text));
        //}
    }
}
