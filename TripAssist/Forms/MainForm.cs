﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TripAssist.Debug;
using TripAssist.Forms.CarInfo;
using TripAssist.Forms.Debugging;

namespace TripAssist
{
    public partial class mainForm : Form
    {
        //Debugging
        private ConsoleLogger consoleLogger;
        private ConsoleView consoleView;

        //CarInfo
        CarInfo carInfo;

        public mainForm()
        {
            InitializeComponent();

            //Init the in-app debugger
            consoleView = new ConsoleView();
            consoleLogger = new ConsoleLogger(consoleView.consoleOutputTextBox);
            Console.SetOut(consoleLogger);

            //Init the CarInfo screen
            carInfo = new CarInfo();
        }

        /// <summary>
        /// Button event that shows the form that allows Console.Write... output to be seen
        /// </summary>
        private void consoleViewButton_Click(object sender, EventArgs e)
        {
            consoleView.Show();
        }

        private void carInfoButton_Click(object sender, EventArgs e)
        {
            carInfo.Show();
        }
    }
}
