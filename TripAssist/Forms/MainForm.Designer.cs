﻿namespace TripAssist
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.consoleViewButton = new System.Windows.Forms.Button();
            this.carInfoButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // consoleViewButton
            // 
            this.consoleViewButton.Location = new System.Drawing.Point(12, 12);
            this.consoleViewButton.Name = "consoleViewButton";
            this.consoleViewButton.Size = new System.Drawing.Size(100, 100);
            this.consoleViewButton.TabIndex = 0;
            this.consoleViewButton.Text = "View Console";
            this.consoleViewButton.UseVisualStyleBackColor = true;
            this.consoleViewButton.Click += new System.EventHandler(this.consoleViewButton_Click);
            // 
            // carInfoButton
            // 
            this.carInfoButton.Location = new System.Drawing.Point(119, 13);
            this.carInfoButton.Name = "carInfoButton";
            this.carInfoButton.Size = new System.Drawing.Size(100, 100);
            this.carInfoButton.TabIndex = 1;
            this.carInfoButton.Text = "Car Info";
            this.carInfoButton.UseVisualStyleBackColor = true;
            this.carInfoButton.Click += new System.EventHandler(this.carInfoButton_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(233, 135);
            this.Controls.Add(this.carInfoButton);
            this.Controls.Add(this.consoleViewButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mainForm";
            this.Text = "Trip Assist";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button consoleViewButton;
        private System.Windows.Forms.Button carInfoButton;
    }
}

