﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TripAssist.Forms.Debugging
{
    public partial class ConsoleView : Form
    {
        public ConsoleView()
        {
            InitializeComponent();
        }

        //Intercept window close and hide instead
        private void ConsoleView_FormClosing(object sender, FormClosingEventArgs e) 
        {
            this.Hide();
            e.Cancel = true;
        }
    }
}
