﻿namespace TripAssist.Forms.Debugging
{
    partial class ConsoleView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.consoleOutputTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // consoleOutputTextBox
            // 
            this.consoleOutputTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.consoleOutputTextBox.BackColor = System.Drawing.SystemColors.MenuText;
            this.consoleOutputTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.consoleOutputTextBox.ForeColor = System.Drawing.SystemColors.Window;
            this.consoleOutputTextBox.Location = new System.Drawing.Point(13, 13);
            this.consoleOutputTextBox.Name = "consoleOutputTextBox";
            this.consoleOutputTextBox.ReadOnly = true;
            this.consoleOutputTextBox.Size = new System.Drawing.Size(545, 213);
            this.consoleOutputTextBox.TabIndex = 0;
            this.consoleOutputTextBox.Text = "";
            // 
            // ConsoleView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(570, 238);
            this.Controls.Add(this.consoleOutputTextBox);
            this.Name = "ConsoleView";
            this.Text = "ConsoleView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConsoleView_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.RichTextBox consoleOutputTextBox;
    }
}