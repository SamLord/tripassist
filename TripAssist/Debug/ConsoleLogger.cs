﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TripAssist.Debug
{
    /// <summary>
    /// Allows Console.Write() to be intercepted by passing a ConsoleLogger object into Console.SetOut
    /// </summary>
    class ConsoleLogger : TextWriter
    {
        RichTextBox _output = null;

        /// <summary>
        /// Create an instance of ConsoleLogger
        /// </summary>
        /// <param name="output">The textbox to output data to</param>
        public ConsoleLogger(RichTextBox output)
        {
            if(output == null)
            {
                throw new ArgumentException("The output RichTextBox cannot be null!");
            }
            _output = output;
        }

        public override void Write(char value)
        {
            base.Write(value);
            _output.AppendText(value.ToString()); // When character data is written, append it to the text box.
        }

        public override Encoding Encoding
        {
            get { return System.Text.Encoding.UTF8; }
        }
    }
}
